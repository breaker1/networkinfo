<?php

declare(strict_types=1);

namespace App\Controller;

use Iodev\Whois\Whois;
use Novutec\WhoisParser\Exception\NoQueryException;
use Novutec\WhoisParser\Parser;
use RemotelyLiving\PHPDNS\Resolvers\Exceptions\ReverseLookupFailure;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class InfoController extends AbstractController
{
    /**
     * @Route("/", name="info")
     */
    public function index()
    {
        $ipAddress = $_SERVER['REMOTE_ADDR'];

        $payload = [
            'ipaddress' => $ipAddress,
            'hostname' => $this->getHostName($ipAddress),
        ];

        $payload['whois'] = $this->getWhois($ipAddress);

        return $this->json($payload);
    }

    /**
     * @param string $ipaddress
     * @return string|null
     * @throws ReverseLookupFailure
     */
    private function getHostName(string $ipaddress): ?string
    {
        $resolver = new \RemotelyLiving\PHPDNS\Resolvers\LocalSystem();
        try {
            return $resolver->getHostnameByAddress($ipaddress)->getHostnameWithoutTrailingDot();
        } catch (ReverseLookupFailure $exception) {
            return null;
        }
    }

    /**
     * @param string $ipAddress
     * @return array|null
     */
    private function getWhois(string $ipAddress): ?array
    {
        $parser = new Parser('object');
        try {
            $response = (array)$parser->lookup($ipAddress);
            unset($response['rawdata']);
            return $response;
        } catch (NoQueryException $exception) {
            return null;
        }
    }
}
